import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/REST_API_Test_Suite')

suiteProperties.put('name', 'REST_API_Test_Suite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\ranji\\Katalon Studio\\CATS_API\\Reports\\REST_API_Test_Suite\\20190226_165333\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/REST_API_Test_Suite', suiteProperties, [new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 1', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'false' , 'query' : 'select PROD_CLS_NBR From  MASTER.PROD_CLS_T where GLOBAL_FLG=\'N\' and PROD_CLS_PAR_NBR is not null and STOP_DT is null order By PROD_CLS_NBR' , 'activeOnly' : 'true' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 2', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'true' , 'query' : 'select PROD_CLS_NBR From  MASTER.PROD_CLS_T where GLOBAL_FLG=\'y\' and PROD_CLS_PAR_NBR is not null and STOP_DT is null order By PROD_CLS_NBR' , 'activeOnly' : 'true' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 3', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'false' , 'query' : 'select PROD_CLS_NBR From  MASTER.PROD_CLS_T where GLOBAL_FLG=\'N\' and PROD_CLS_PAR_NBR is not null order By PROD_CLS_NBR' , 'activeOnly' : 'false' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 4', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'true' , 'query' : 'select PROD_CLS_NBR From  MASTER.PROD_CLS_T where GLOBAL_FLG=\'y\' and PROD_CLS_PAR_NBR is not null and STOP_DT is null order By PROD_CLS_NBR' , 'activeOnly' : 'false' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 5', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : '' , 'query' : '' , 'activeOnly' : '' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 6', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : '' , 'query' : '' , 'activeOnly' : '' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 7', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'Global' , 'query' : 'Remarks' , 'activeOnly' : 'Active' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 8', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'efg' , 'query' : '' , 'activeOnly' : 'abc' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 9', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : '!(&#&' , 'query' : '' , 'activeOnly' : '@#(&#' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 10', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'null' , 'query' : '' , 'activeOnly' : 'null' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 11', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'false' , 'query' : '' , 'activeOnly' : 'true' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters - Iteration 12', 'Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters',  [ 'globalsOnly' : 'false' , 'query' : '' , 'activeOnly' : 'true' ,  ])])
