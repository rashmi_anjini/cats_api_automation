import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/REST_API_TEST_SUITE/TargetMethods_Test_Suite')

suiteProperties.put('name', 'TargetMethods_Test_Suite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\ranji\\Katalon Studio\\CATS_API\\Reports\\REST_API_TEST_SUITE\\TargetMethods_Test_Suite\\20190228_164012\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/REST_API_TEST_SUITE/TargetMethods_Test_Suite', suiteProperties, [new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR - Iteration 1', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'N\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'FR' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR - Iteration 2', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'Y\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'FR' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR - Iteration 3', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'N\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'FR' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR - Iteration 4', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Repsonse_FR',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'Y\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'FR' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP - Iteration 1', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'N\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'JP' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP - Iteration 2', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'Y\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'JP' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP - Iteration 3', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'N\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'JP' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP - Iteration 4', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_JP',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'Y\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'JP' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK - Iteration 1', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'N\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK - Iteration 2', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'Y\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK - Iteration 3', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'N\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK - Iteration 4', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_UK',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t\nwhere TGTNG_METH_NBR not in (\nselect distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm\njoin MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'Y\' \norder by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US - Iteration 1', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t where TGTNG_METH_NBR not in (select distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm join MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'N\' order by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'US' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US - Iteration 2', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t where TGTNG_METH_NBR not in (select distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm join MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'Y\' order by TGTNG_METH_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'US' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US - Iteration 3', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t where TGTNG_METH_NBR not in (select distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm join MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null) and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'N\' order by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'US' ,  ]), new TestCaseBinding('Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US - Iteration 4', 'Test Cases/TargetMethods/VALID_Cases/Verify_TM_StatusCode_Response_US',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.TGTNG_METH_NBR,t.TGTNG_METH_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.TGTNG_METH_T t where TGTNG_METH_NBR not in (select distinct b.TGTNG_METH_PAR_NBR from MASTER.TGTNG_METH_T atm join MASTER.TGTNG_METH_T b on atm.TGTNG_METH_NBR = b.TGTNG_METH_NBR\nwhere b.TGTNG_METH_PAR_NBR is not null) and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'Y\' order by TGTNG_METH_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'US' ,  ])])
