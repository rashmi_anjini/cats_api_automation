import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/REST_API_TEST_SUITE/ProductClasse_Test_Suite')

suiteProperties.put('name', 'ProductClasse_Test_Suite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\ranji\\Katalon Studio\\CATS_API\\Reports\\REST_API_TEST_SUITE\\ProductClasse_Test_Suite\\20190228_010648\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/REST_API_TEST_SUITE/ProductClasse_Test_Suite', suiteProperties, [new TestCaseBinding('Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK - Iteration 1', 'Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK',  [ 'globalsOnly' : 'false' , 'query' : 'select distinct t.PROD_CLS_NBR,t.PROD_CLS_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.PROD_CLS_T t\nwhere PROD_CLS_NBR not in (\nselect distinct b.PROD_CLS_PAR_NBR from MASTER.PROD_CLS_T atm\njoin MASTER.PROD_CLS_T b on atm.PROD_CLS_NBR = b.PROD_CLS_NBR\nwhere b.PROD_CLS_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'N\' \norder by PROD_CLS_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK - Iteration 2', 'Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK',  [ 'globalsOnly' : 'true' , 'query' : 'select distinct t.PROD_CLS_NBR,t.PROD_CLS_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.PROD_CLS_T t\nwhere PROD_CLS_NBR not in (\nselect distinct b.PROD_CLS_PAR_NBR from MASTER.PROD_CLS_T atm\njoin MASTER.PROD_CLS_T b on atm.PROD_CLS_NBR = b.PROD_CLS_NBR\nwhere b.PROD_CLS_PAR_NBR is not null\n)and ( t.stop_dt is null or t.stop_dt >= current_date) and t.GLOBAL_FLG = \'Y\' \norder by PROD_CLS_NBR' , 'activeOnly' : 'true' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK - Iteration 3', 'Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK',  [ 'globalsOnly' : 'false' , 'query' : 'Select distinct t.PROD_CLS_NBR,t.PROD_CLS_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.PROD_CLS_T t\nwhere PROD_CLS_NBR not in (\nselect distinct b.PROD_CLS_PAR_NBR from MASTER.PROD_CLS_T atm\njoin MASTER.PROD_CLS_T b on atm.PROD_CLS_NBR = b.PROD_CLS_NBR\nwhere b.PROD_CLS_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'N\' order by PROD_CLS_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'UK' ,  ]), new TestCaseBinding('Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK - Iteration 4', 'Test Cases/ProductClasses/VALID_Cases/Verify_PC_StatusCode_and_Response_UK',  [ 'globalsOnly' : 'true' , 'query' : 'Select distinct t.PROD_CLS_NBR,t.PROD_CLS_PAR_NBR,t.STOP_DT,t.GLOBAL_FLG from MASTER.PROD_CLS_T t\nwhere PROD_CLS_NBR not in (\nselect distinct b.PROD_CLS_PAR_NBR from MASTER.PROD_CLS_T atm\njoin MASTER.PROD_CLS_T b on atm.PROD_CLS_NBR = b.PROD_CLS_NBR\nwhere b.PROD_CLS_PAR_NBR is not null\n)and ( t.stop_dt is not null or t.stop_dt < current_date) and t.GLOBAL_FLG = \'Y\' order by PROD_CLS_NBR' , 'activeOnly' : 'false' , 'managingCountryCode' : 'UK' ,  ])])
