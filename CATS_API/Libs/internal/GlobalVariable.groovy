package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object testCount
     
    /**
     * <p></p>
     */
    public static Object testCountUK
     
    /**
     * <p></p>
     */
    public static Object testCountFR
     
    /**
     * <p></p>
     */
    public static Object testCountJP
     
    /**
     * <p></p>
     */
    public static Object testCountIN
     
    /**
     * <p></p>
     */
    public static Object access_token
     
    /**
     * <p></p>
     */
    public static Object testPlanRunId
     
    /**
     * <p></p>
     */
    public static Object TestCaseRunId
     
    /**
     * <p></p>
     */
    public static Object TestCaseStatus
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['testCount' : 0, 'testCountUK' : 7, 'testCountFR' : 14, 'testCountJP' : 21, 'testCountIN' : 0, 'access_token' : 'NzE0OnorcW1BY2l1MHBkYmdqK0JWbC9DZjFWY0VKOTh3a2JRK3Z6eDVBZ0RPZlE9', 'testPlanRunId' : 54742, 'TestCaseRunId' : 12345, 'TestCaseStatus' : [:]])
        
        String profileName = RunConfiguration.getExecutionProfile()
        def selectedVariables = allVariables[profileName]
		
		for(object in selectedVariables){
			String overridingGlobalVariable = RunConfiguration.getOverridingGlobalVariable(object.key)
			if(overridingGlobalVariable != null){
				selectedVariables.put(object.key, overridingGlobalVariable)
			}
		}

        testCount = selectedVariables["testCount"]
        testCountUK = selectedVariables["testCountUK"]
        testCountFR = selectedVariables["testCountFR"]
        testCountJP = selectedVariables["testCountJP"]
        testCountIN = selectedVariables["testCountIN"]
        access_token = selectedVariables["access_token"]
        testPlanRunId = selectedVariables["testPlanRunId"]
        TestCaseRunId = selectedVariables["TestCaseRunId"]
        TestCaseStatus = selectedVariables["TestCaseStatus"]
        
    }
}
