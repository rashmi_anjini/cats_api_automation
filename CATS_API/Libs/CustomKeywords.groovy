
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.ResponseObject

import java.util.List

import java.lang.String

import org.apache.poi.xssf.usermodel.XSSFSheet


def static "com.utility.CommonMethods.verifyObjectCount"(
    	ResponseObject response	
     , 	int expectedCount	) {
    (new com.utility.CommonMethods()).verifyObjectCount(
        	response
         , 	expectedCount)
}

def static "com.utility.CommonMethods.parseResponseJson"(
    	ResponseObject response	) {
    (new com.utility.CommonMethods()).parseResponseJson(
        	response)
}

def static "com.utility.CommonMethods.simpleDateTimeFormat"() {
    (new com.utility.CommonMethods()).simpleDateTimeFormat()
}

def static "com.utility.CommonMethods.VerifyElementsinDatabaseandResponse"(
    	List parsedData	
     , 	int responsecount	
     , 	int dbCount	
     , 	List records	
     , 	String testCaseName	) {
    (new com.utility.CommonMethods()).VerifyElementsinDatabaseandResponse(
        	parsedData
         , 	responsecount
         , 	dbCount
         , 	records
         , 	testCaseName)
}

def static "com.utility.CommonMethods.setUpLoggers"() {
    (new com.utility.CommonMethods()).setUpLoggers()
}

def static "com.utility.CommonMethods.VerifyElementsinTargetmethods"(
    	Object parsedData	
     , 	int responsecount	
     , 	int dbCount	
     , 	List records	
     , 	String testCaseName	) {
    (new com.utility.CommonMethods()).VerifyElementsinTargetmethods(
        	parsedData
         , 	responsecount
         , 	dbCount
         , 	records
         , 	testCaseName)
}

def static "databaseConnection.execSQL"(
    	String host	
     , 	String port	
     , 	String sid	
     , 	String username	
     , 	String password	
     , 	String query	) {
    (new databaseConnection()).execSQL(
        	host
         , 	port
         , 	sid
         , 	username
         , 	password
         , 	query)
}

def static "com.utility.ReadExcel.getCellData"(
    	int RowNum	
     , 	int ColNum	
     , 	String SheetName	) {
    (new com.utility.ReadExcel()).getCellData(
        	RowNum
         , 	ColNum
         , 	SheetName)
}

def static "com.utility.ReadExcel.getSheet"(
    	String sheetname	) {
    (new com.utility.ReadExcel()).getSheet(
        	sheetname)
}

def static "com.utility.ReadExcel.getCellData"(
    	XSSFSheet sheet	
     , 	int RowNum	
     , 	int ColNum	) {
    (new com.utility.ReadExcel()).getCellData(
        	sheet
         , 	RowNum
         , 	ColNum)
}

def static "com.utility.ReadExcel.getCellDate"(
    	XSSFSheet sheet	
     , 	int RowNum	
     , 	int ColNum	) {
    (new com.utility.ReadExcel()).getCellDate(
        	sheet
         , 	RowNum
         , 	ColNum)
}

def static "com.utility.WriteExcel.setCellData"(
    	Object Result	
     , 	int RowNum	
     , 	int ColNum	
     , 	String Sheetname	) {
    (new com.utility.WriteExcel()).setCellData(
        	Result
         , 	RowNum
         , 	ColNum
         , 	Sheetname)
}

def static "com.utility.UpdateTargetProcess.UpdateTestCaseStatus"(
    	String testCaseName	
     , 	String status	) {
    (new com.utility.UpdateTargetProcess()).UpdateTestCaseStatus(
        	testCaseName
         , 	status)
}
