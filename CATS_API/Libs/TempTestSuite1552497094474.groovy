import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/CATS_REST_API/UpdatingTarget_Process')

suiteProperties.put('name', 'UpdatingTarget_Process')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\ranji\\Katalon Studio\\CATS_API\\Reports\\CATS_REST_API\\UpdatingTarget_Process\\20190313_224134\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/CATS_REST_API/UpdatingTarget_Process', suiteProperties, [new TestCaseBinding('Test Cases/TargetProcess/UpdateTestCaseResults', 'Test Cases/TargetProcess/UpdateTestCaseResults',  null), new TestCaseBinding('Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_FR', 'Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_FR',  null), new TestCaseBinding('Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_JP', 'Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_JP',  null), new TestCaseBinding('Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_UK', 'Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_UK',  null), new TestCaseBinding('Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_US', 'Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_US',  null), new TestCaseBinding('Test Cases/Product_Classes/Verify_PC_StatusCode_For_Invalid_Parameters', 'Test Cases/Product_Classes/Verify_PC_StatusCode_For_Invalid_Parameters',  null), new TestCaseBinding('Test Cases/Target_Methods/Verify_TM_StatusCode_For_Invalid_Parameters', 'Test Cases/Target_Methods/Verify_TM_StatusCode_For_Invalid_Parameters',  null), new TestCaseBinding('Test Cases/Target_Methods/Verify_TM_StatusCode_Response_FR', 'Test Cases/Target_Methods/Verify_TM_StatusCode_Response_FR',  null), new TestCaseBinding('Test Cases/Target_Methods/Verify_TM_StatusCode_Response_JP', 'Test Cases/Target_Methods/Verify_TM_StatusCode_Response_JP',  null), new TestCaseBinding('Test Cases/Target_Methods/Verify_TM_StatusCode_Response_UK', 'Test Cases/Target_Methods/Verify_TM_StatusCode_Response_UK',  null), new TestCaseBinding('Test Cases/Target_Methods/Verify_TM_StatusCode_Response_US', 'Test Cases/Target_Methods/Verify_TM_StatusCode_Response_US',  null)])
