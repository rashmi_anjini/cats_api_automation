<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Product_Classes</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>bb040b2f-596e-47f3-9233-fcf014d2b91c</testSuiteGuid>
   <testCaseLink>
      <guid>ec277612-0b66-4362-83af-51973ff8c177</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product_Classes/UpdateTestCaseResults_PC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d8ecdad-29f4-4a41-b0d7-0d800a960660</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_FR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aafa3391-03e9-4c22-845b-1d2d816a7365</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_JP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea04dd00-1f3d-47b4-a8b7-57c0c71fc6b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_UK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97dff33f-7dc5-4e21-9456-036ad072ae71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product_Classes/Verify_PC_StatusCode_and_Response_US</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d42fdbc0-8454-45ef-bace-121033b0ec0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product_Classes/Verify_PC_StatusCode_For_Invalid_Parameters</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
