<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Target_Methods</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>713a4462-7724-44b1-8c3a-14822cc06a56</testSuiteGuid>
   <testCaseLink>
      <guid>9c4b3e65-254d-442b-b927-fff29b4a0647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Target_Methods/UpdateTestCaseResults_TM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d980089-9be8-43e4-8025-89ced9669f4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Target_Methods/Verify_TM_StatusCode_For_Invalid_Parameters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86b87f6c-6829-47fa-8337-c81a1e755be1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Target_Methods/Verify_TM_StatusCode_Response_FR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99d0000d-85b7-49a4-8c5a-d03f7448cd9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Target_Methods/Verify_TM_StatusCode_Response_JP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a397998-99c2-483a-b9c8-6548a64073ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Target_Methods/Verify_TM_StatusCode_Response_UK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fc18fd9-0b36-47c0-8509-9d95e1234a32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Target_Methods/Verify_TM_StatusCode_Response_US</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
