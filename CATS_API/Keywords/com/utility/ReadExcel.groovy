package com.utility

import java.text.SimpleDateFormat

import org.apache.poi.hssf.usermodel.HSSFDateUtil
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.kms.katalon.core.annotation.Keyword

public class ReadExcel {


	@Keyword
	def getSheet(String sheetname){
		
		def projectpath = System.getProperty("user.dir")
		projectpath = projectpath + "\\API_Parameters.xlsx"
		
		File xlsfile = new File(projectpath)
		FileInputStream file = new FileInputStream (xlsfile)

		XSSFWorkbook workbook = new XSSFWorkbook(file)

		return workbook.getSheet(sheetname)
	}

	@Keyword
	def getCellData(XSSFSheet sheet , int RowNum, int ColNum){

		XSSFRow Row = sheet.getRow(RowNum)
		if(Row == null) {
			return null
		}
		XSSFCell Cell = Row.getCell(ColNum)
		if(Cell == null) {
			return null
		}
		String CellData = Cell.stringCellValue
		return Cell.stringCellValue
	}

	@Keyword
	def getCellDate(XSSFSheet sheet , int RowNum, int ColNum) {

		XSSFRow Row = sheet.getRow(RowNum)
		XSSFCell Cell = Row.getCell(ColNum)
		if (HSSFDateUtil.isCellDateFormatted(Cell)) {

			def date=   Cell.getDateCellValue()
			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd");
			def strCellValue = dateFormat.format(date);
			return strCellValue
		}
	}
}
