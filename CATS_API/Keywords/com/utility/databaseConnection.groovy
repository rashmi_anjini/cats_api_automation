

import java.sql.ResultSet

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

import groovy.sql.Sql

public class databaseConnection {


	@Keyword

	def List execSQL(String host, String port, String sid, String username, String password, String query) {

		String connectionString
		KeywordLogger logger = new KeywordLogger()

		connectionString = "jdbc:oracle:thin:@" + host + ":" + port + ":" + sid

		def sql = Sql.newInstance(connectionString, username, password)
		def prodNum = []
		try {
			//sql.eachRow(query){ result ->
			//prodNum.add(result);
			//}

			prodNum = sql.rows(query)
		} finally {

			sql.close()
		}
		return prodNum
	}
}
