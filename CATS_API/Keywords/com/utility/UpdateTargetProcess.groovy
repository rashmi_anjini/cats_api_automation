package com.utility
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS


import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger
import org.apache.logging.log4j.core.LoggerContext

import internal.GlobalVariable


class UpdateTargetProcess {


	Logger logger = LogManager.getLogger("TrgtPross")
	@Keyword
	def String UpdateTestCaseStatus(String testCaseName, String status) {

		def testCaseRunId = GlobalVariable.TestCaseStatus[testCaseName]

		def request = (RequestObject)findTestObject('TargetProcessEndPoints/UpdateTestCaseResult', [('access_token') : GlobalVariable.access_token
			, ('testCaseRunId') : testCaseRunId])

		logger.info("Request Information : https://catalina.tpondemand.com/api/v1/TestCaseRuns/"+testCaseRunId+"?access_token="+GlobalVariable.access_token  )
		//  logger.info('test plan ' + parsedJson.Items[i].TestPlanRun.Id +' testCaseRunId '+ parsedJson.Items[i].Id + ' test case id: ' + parsedJson.Items[i].TestCase.Id + ' test case name: ' + parsedJson.Items[i].TestCase.Name)

		/*String body 1= "{ \"Status\": \""
		 body1 = body1 + status
		 body1 = body1 +"\",\"Comment\":\"Test execution completed\"}"
		 //WS.comment(body)*/
		com.utility.CommonMethods dateTime = new com.utility.CommonMethods()
		def startDate= dateTime.simpleDateTimeFormat()
		String body= """{ "Status": "${status}",	"Comment":"Test execution completed"}"""


		logger.info("Test case name :"+testCaseName+" status: "+status)
		logger.info(body)

		request.setBodyContent(new HttpTextBodyContent(body,"UTF-8", "application/json"))
		WS.sendRequest(request)
	}
}