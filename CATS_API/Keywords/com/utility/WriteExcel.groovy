package com.utility

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.lang.String
public class WriteExcel {
	@Keyword

	def void setCellData(def Result,  int RowNum, int ColNum, String Sheetname) throws Exception {

		try{
			FileInputStream file = new FileInputStream (new File('C://Users//ranji//Katalon Studio//CATS_API//API_Parameters.xlsx'))

			XSSFWorkbook workbook = new XSSFWorkbook(file)

			XSSFSheet sheet = workbook.getSheet(Sheetname)

			XSSFRow Row  =sheet.getRow(RowNum)

			XSSFCell Cell = Row.getCell(ColNum,Row.RETURN_BLANK_AS_NULL)

			if (Cell == null) {
				Cell = Row.createCell(ColNum)


				Cell.setCellValue(Result)
			} else {

				Cell.setCellValue(Result)
			}


			FileOutputStream fileOut = new FileOutputStream('C://Users//ranji//Katalon Studio//CATS_API//API_Parameters.xlsx')

			workbook.write(fileOut)

			fileOut.flush()

			fileOut.close()
		}catch(Exception e){

			throw (e);
		}
	}
}

