package com.utility

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger
import org.apache.logging.log4j.core.LoggerContext

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ResponseObject

import groovy.json.JsonSlurper
import internal.GlobalVariable

class CommonMethods {


	@Keyword

	Logger logger = LogManager.getLogger("Com_Methds")

	def int verifyStatusCode(ResponseObject response, int expectedStatusCode, String testCaseName) {
		int statusCode = response.getStatusCode()
		logger.info("status code is :" +statusCode)
		if (response.getStatusCode() == expectedStatusCode) {
			logger.info("Response status codes match")
			assert true
		} else {
			logger.error("Response status code not match. Expected: " +
					expectedStatusCode + " - Actual: " + response.getStatusCode())
			com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
			api.UpdateTestCaseStatus(testCaseName, "Failed")
			assert false
		}
		return statusCode
	}




	@Keyword
	def int verifyObjectCount(ResponseObject response, int expectedCount) {
		JsonSlurper slurper = new JsonSlurper()
		List parsedJson = slurper.parseText(response.getResponseBodyContent())
		int count = parsedJson.size()
		logger.info('Response Count :' + count.toString())
		if (count == expectedCount) {
			logger.info("Response count  match")
		} else {
			logger.error("Response count not match. Expected: " +expectedCount + " - Actual: " + count )
		}
		return count
	}

	@Keyword
	def List parseResponseJson(ResponseObject response) {
		JsonSlurper slurper = new JsonSlurper()
		List parsedJson = slurper.parseText(response.getResponseBodyContent())
		int count = parsedJson.size()
		return parsedJson
	}


	@Keyword
	def simpleDateTimeFormat() {
		def now = new Date()
		String formattedDate= now.format("dd-MMM-yyyy HH:MM")
		return formattedDate
	}

	@Keyword
	def VerifyElementsinDatabaseandResponse(List parsedData, int responsecount,int dbCount, List records, String testCaseName) {
		if(responsecount == dbCount) {
			for(int i=0;i<parsedData.size();i++) {
				if(parsedData[i].ProdNumber.toString() != records[i].PROD_CLS_NBR.toString()) {
					logger.info('Expected ' + parsedData[i].ProdNumber + ' Actual '+ records[i].PROD_CLS_NBR)
				}
				if(parsedData[i].ProdParentNumber.toString() != records[i].PROD_CLS_PAR_NBR.toString()) {
					logger.info('Expected ' + parsedData[i].ProdParentNumber + ' Actual '+ records[i].PROD_CLS_PAR_NBR)
				}
				if(parsedData[i].StopDate != records[i].STOP_DT) {
					logger.info('Expected ' + parsedData[i].StopDate + ' Actual '+ records[i].STOP_DT)
				}
				if(parsedData[i].GlobalFlag.toString() != records[i].GLOBAL_FLG.toString()) {
					logger.info('Expected ' + parsedData[i].GlobalFlag + ' Actual '+ records[i].GLOBAL_FLG)
				}

				if(parsedData[i].ProdNumber.toString() != records[i].PROD_CLS_NBR.toString() ||
				parsedData[i].ProdParentNumber.toString() != records[i].PROD_CLS_PAR_NBR.toString() ||
				parsedData[i].StopDate != records[i].STOP_DT ||
				parsedData[i].GlobalFlag.toString() != records[i].GLOBAL_FLG.toString()) {
					logger.error('Expected and actual data value do not match')
					logger.info('Expected ' + parsedData[i].ProdNumber + ' Actual '+ records[i].PROD_CLS_NBR)
					logger.info('Expected ' + parsedData[i].ProdParentNumber + ' Actual '+ records[i].PROD_CLS_PAR_NBR)
					logger.info('Expected ' + parsedData[i].StopDate + ' Actual '+ records[i].STOP_DT)
					logger.info('Expected ' + parsedData[i].GlobalFlag + ' Actual '+ records[i].GLOBAL_FLG)
					com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
					api.UpdateTestCaseStatus(testCaseName, "Failed")
					assert false
				}
			}
			logger.info('Expected and actual data value match')
			assert true
		}
	}

	@Keyword

	def setUpLoggers() {
		LoggerContext context = (LoggerContext)LogManager.getContext(false)
		def projectpath = System.getProperty("user.dir")
		projectpath = projectpath + "/Include/config/log4j.xml"
		File file = new File(projectpath)
		context.setConfigLocation(file.toURI())
		println("Log file path: "+context.configLocation.path)
	}



	@Keyword
	def VerifyElementsinTargetmethods(def parsedData, int responsecount,int dbCount, List records,String testCaseName) {
		if(responsecount == dbCount) {
			parsedData = parsedData.sort { a,b -> a.TargetingMethodNumber <=> b.TargetingMethodNumber}

			for(int i=0;i<parsedData.size();i++) {

				if(parsedData[i].TargetingMethodNumber.toString() != records[i].TGTNG_METH_NBR.toString()) {
					logger.info('Expected ' + parsedData[i].TargetingMethodNumber + ' Actual '+ records[i].TGTNG_METH_NBR)
				}
				if(parsedData[i].TargetingMethodParentNumber.toString() != records[i].TGTNG_METH_PAR_NBR.toString()) {
					logger.info('Expected ' + parsedData[i].TargetingMethodParentNumber + ' Actual '+ records[i].TGTNG_METH_PAR_NBR)
				}
				if(parsedData[i].StopDate != records[i].STOP_DT) {
					logger.info('Expected ' + parsedData[i].StopDate + ' Actual '+ records[i].STOP_DT)
				}
				if(parsedData[i].GlobalFlag.toString() != records[i].GLOBAL_FLG.toString()) {
					logger.info('Expected ' + parsedData[i].GlobalFlag + ' Actual '+ records[i].GLOBAL_FLG)
				}

				if(parsedData[i].TargetingMethodNumber.toString() != records[i].TGTNG_METH_NBR.toString() ||
				parsedData[i].TargetingMethodParentNumber.toString() != records[i].TGTNG_METH_PAR_NBR.toString() ||
				parsedData[i].StopDate != records[i].STOP_DT ||
				parsedData[i].GlobalFlag.toString() != records[i].GLOBAL_FLG.toString()) {
					logger.error('Expected and actual data value do not match')
					logger.info('Expected TargetingMethodNumber'+[i]+"-" + parsedData[i].TargetingMethodNumber + ' Actual '+ records[i].TGTNG_METH_NBR)
					logger.info('ExpectedTargetingMethodParentNumber '+[i]+"-" + parsedData[i].TargetingMethodParentNumber + ' Actual '+ records[i].TGTNG_METH_PAR_NBR)
					logger.info('Expected StopDate' +[i]+"-"+ parsedData[i].StopDate + ' Actual '+ records[i].STOP_DT)
					logger.info('Expected GlobalFlag ' +[i]+"-"+ parsedData[i].GlobalFlag + ' Actual '+ records[i].GLOBAL_FLG)

					com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
					api.UpdateTestCaseStatus(testCaseName, "Failed")
					assert false
				}
			}
			logger.info('Expected and actual data value match')

			assert true
		}
	}
}
