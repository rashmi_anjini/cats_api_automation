import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS


//select * from C2C_APP.REQ_TYP_AGRMNT_T where REQ_TYP_KEY_NBR=7; for Retailer
CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("savecontract")
logger.info("***********************************Start test case for savecontract ***************************************")
def request = (RequestObject)findTestObject('Contracts/saveContract')
def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('SaveContract')

def ContractDescription	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,1)
def AgreementTypeNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,2)
def ClientNumber		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,3)
def SalesPersonNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,4)
def ContractAmount		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,5)
def ContractStartDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, 2,6)
def ContractStopDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, 2,7)
def ExclusivityType		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,8)
def CategoryOwnership	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,9)
def Categories			= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,10)
def GeographyType		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,11)
def GeogaphyIdentityNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,12)
def StartDate			= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet,2,13)
def StopDate			= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, 2,14)
def TimeFrameType		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,15)
def MasterContractNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,16)
def ProposalNumber		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,17)
def ClientSignature		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,18)
def ClientSignarureDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, 2,19)
def CatalinaSignatureDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, 1,22)
def UserName			= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,24)
def StoreQuantity		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,25)
def EstPrintQuantity	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,26)
def MinimumAmount		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,27)
def MinPrintsIncluded	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,28)
def ProgramSetUpFee		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,29)
def BillContactFirstName	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,31)
def BillContactLastName	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 2,32)
def MiscellaneousCharges	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,2,33)

String strSOAPBody = """<Envelope xmlns="http://www.w3.org/2003/05/soap-envelope">
    					  <Body>
        				    <saveContract xmlns="http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1">
                       		   <request>
							        <ContractDescription>${ContractDescription}</ContractDescription>
							        <AgreementTypeNumber>${AgreementTypeNumber}</AgreementTypeNumber>
							        <ClientNumber>${ClientNumber}</ClientNumber>
							        <SalesPersonNumber>${SalesPersonNumber}</SalesPersonNumber>
							        <ContractAmount>${ContractAmount}</ContractAmount>
							        <ContractStartDate>${ContractStartDate}</ContractStartDate>
							        <ContractStopDate>${ContractStopDate}</ContractStopDate>
							        <Commitment>
          					        <ExclusivityType>${ExclusivityType}</ExclusivityType>
							        <CategoryOwnership>${CategoryOwnership}</CategoryOwnership>
							        <Categories>${Categories}</Categories>
                                    <Chains></Chains>
                   					 <Geographies>
							            <GeographyType>${GeographyType}</GeographyType>
							            <GeogaphyIdentityNumber>${GeogaphyIdentityNumber}</GeogaphyIdentityNumber>
							          </Geographies>
          						    <TimeFrames>
						            <StartDate>${StartDate}</StartDate>
						            <StopDate>${StopDate}</StopDate>
						            <TimeFrameType>${TimeFrameType}</TimeFrameType>
						          </TimeFrames>
                                  </Commitment>
                 				<MasterContractNumber>${MasterContractNumber}</MasterContractNumber>
						        <ProposalNumber>${ProposalNumber}</ProposalNumber>
						        <ClientSignature>${ClientSignature}</ClientSignature>
						        <ClientSignarureDate>${ClientSignarureDate}</ClientSignarureDate>
						        <ClientSignatureTitle></ClientSignatureTitle>
						        <CatalinaSignature></CatalinaSignature>
						        <CatalinaSignatureDate>${CatalinaSignatureDate}</CatalinaSignatureDate>
						        <CatalinaSignatureTitle></CatalinaSignatureTitle>
						        <UserName>${UserName}</UserName>
                				<StoreQuantity>${StoreQuantity}</StoreQuantity>
						        <EstPrintQuantity>${EstPrintQuantity}</EstPrintQuantity>
						        <MinimumAmount>${MinimumAmount}</MinimumAmount>
						        <MinPrintsIncluded>${MinPrintsIncluded}</MinPrintsIncluded>
                				<ProgramSetUpFee>${ProgramSetUpFee}</ProgramSetUpFee>
						        <BillContactKey></BillContactKey>
						        <BillContactFirstName>${BillContactFirstName}</BillContactFirstName>
						        <BillContactLastName>${BillContactLastName}</BillContactLastName>
						        <MiscellaneousCharges>${MiscellaneousCharges}</MiscellaneousCharges>
						        <Header>
						          <RequestingSystem xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate"></RequestingSystem>
						          <CallFromFunction xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate"></CallFromFunction>
						          <User xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate"></User>
						         </Header>
						      </request>
						   </saveContract>
						</Body>
		 </Envelope>"""

	//set soap body and send request and verify status code	
	request.setSoapBody(strSOAPBody)
	def response = WS.sendRequest(request)
    int StatusCode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response, 200,"Verify_saveContract_Details_RTLR")
	logger.info("status code:"+StatusCode)

	//Parse the response to get contract number and write to excel
	XmlParser slurper = new XmlParser()
	def parsedXml = slurper.parseText(response.getResponseBodyContent())
	logger.info(parsedXml)
	def ContractNumber =parsedXml.saveContractResult.ContractNumber.text()
	logger.info("Contract Number is  :" + ContractNumber)
	CustomKeywords.'com.utility.WriteExcel.setCellData'(ContractNumber, 1, 0, "InsertBill")
	WS.delay(5)	
	
	//validate the Contract number in DB
	
	def query= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 1,37)
	String stmt = query+"="+"${ContractNumber}"
	logger.info(stmt)
	List records = CustomKeywords.'databaseConnection.execSQL'('TOCUSQA3', '1521', 'TOCUSQA3', 'RANJI', 'RANJI126', stmt)
	int dbCount = records.size()
	logger.info('Database Count :' + dbCount)
	def ActualContractNumber=records.CON_NBR.toString().replaceAll("\\[", "").replaceAll("\\]", "") 
	
	def testCaseName="Verify_saveContract_Details_RTLR"
	if (ActualContractNumber == ContractNumber) {
		logger.info("Expected and actual contract number match")
		assert true
	} else {
		logger.error("Expected and actual contract number do not match: Expected "+ ContractNumber + ' Actual ' + ActualContractNumber)
		com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
		api.UpdateTestCaseStatus(testCaseName, "Failed")
		assert false
	}
	

	
	//Validate Program Fee Amount is equal to Contract amount in API call
	def ProgramFeeAmount =records.PGM_FEE_AMT.toString().replaceAll("\\[", "").replaceAll("\\]", "") 
	if(ContractAmount==ProgramFeeAmount){
		logger.info("Program Fee amount is same as Contract amount from APi cal " + ContractAmount + ' ProgramFeeAmount ' + ProgramFeeAmount)
		assert true
	} else {
		logger.error("Program Fee amount is not same as Contract amount from APi cal: ContractAmount "+ ContractAmount + ' ProgramFeeAmount ' + ProgramFeeAmount)
		com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
		api.UpdateTestCaseStatus(testCaseName, "Failed")
		assert false
	}
		
	/*
	//Validate Contract Amount = Program Fee Amount + ProgramSetUpFee + MiscellaneousCharges + MinimumAmount(if included)
	
	def ContractAmountDB=records.CON_AMT.toString().replaceAll("\\[", "").replaceAll("\\]", "") 
	def MiscellaneousChargesDB=records.MISC_FEE_AMT.toString().replaceAll("\\[", "").replaceAll("\\]", "")
	
	def MinimumAmountDB=records.MIN_AMT.toString().replaceAll("\\[", "").replaceAll("\\]", "")
	
	TotalContractAmount= Integer.parseInt(ProgramFeeAmount)+ Integer.parseInt(MiscellaneousChargesDB)+ Integer.parseInt(MinimumAmount)
	
	if(ContractAmountDB==TotalContractAmount){
		logger.info("TotalContractAmount : " +TotalContractAmount+"and ContractAmountDB matches: " +ContractAmountDB )
		assert true
	} else {
		logger.error("TotalContractAmount : " +TotalContractAmount+ " and ContractAmountDB do not matches: " +ContractAmountDB )
		com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
		api.UpdateTestCaseStatus(testCaseName, "Failed")
		assert false
	}*/
	
	CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_saveContract_Details", "Passed")
	logger.info("***********************************End test case for saveContract ***************************************")
	import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

