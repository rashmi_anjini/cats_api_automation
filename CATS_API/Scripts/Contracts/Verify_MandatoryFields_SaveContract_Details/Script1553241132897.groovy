import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS


CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("savecontract")
def testCaseName="Verify_MandatoryFields_SaveContract_Details"
logger.info("***********************************Start test case for Mandatory fields validations***************************************")
def request = (RequestObject)findTestObject('Contracts/saveContract')
def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('SaveContract')

for(int i=7; i<=7;i++)
{
		def ContractDescription	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,1)
		logger.info([i]+ ContractDescription)
		def AgreementTypeNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,2)
		def ClientNumber		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,3)
		def SalesPersonNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,4)
		def ContractAmount		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,5)
		def ContractStartDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, i,6)
		def ContractStopDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, i,7)
		def ExclusivityType		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,8)
		def CategoryOwnership	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,9)
		def Categories			= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,10)
		def GeographyType		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,11)
		def GeogaphyIdentityNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,12)
		def StartDate			= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, i,13)
		def StopDate			= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, i,14)
		def TimeFrameType		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,15)
		def MasterContractNumber	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,16)
		def ProposalNumber		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,17)
		def ClientSignature		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,18)
		def ClientSignarureDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, i,19)
		def CatalinaSignatureDate	= CustomKeywords.'com.utility.ReadExcel.getCellDate'(sheet, i,22)
		def UserName			= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,24)
		def StoreQuantity		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,25)
		def EstPrintQuantity	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,26)
		def MinimumAmount		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,27)
		def MinPrintsIncluded	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,28)
		def ProgramSetUpFee		= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,29)
		def BillContactFirstName	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,31)
		def BillContactLastName	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,32)
		def MiscellaneousCharges	= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, i,33)
		
		String strSOAPBody = """<Envelope xmlns="http://www.w3.org/2003/05/soap-envelope">
		    					  <Body>
		        				    <saveContract xmlns="http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1">
		                       		   <request>
									        <ContractDescription>${ContractDescription}</ContractDescription>
									        <AgreementTypeNumber>${AgreementTypeNumber}</AgreementTypeNumber>
									        <ClientNumber>${ClientNumber}</ClientNumber>
									        <SalesPersonNumber>${SalesPersonNumber}</SalesPersonNumber>
									        <ContractAmount>${ContractAmount}</ContractAmount>
									        <ContractStartDate>${ContractStartDate}</ContractStartDate>
									        <ContractStopDate>${ContractStopDate}</ContractStopDate>
									        <Commitment>
		          					        <ExclusivityType>${ExclusivityType}</ExclusivityType>
									        <CategoryOwnership>${CategoryOwnership}</CategoryOwnership>
									        <Categories>${Categories}</Categories>
		                                    <Chains></Chains>
		                   					 <Geographies>
									            <GeographyType>${GeographyType}</GeographyType>
									            <GeogaphyIdentityNumber>${GeogaphyIdentityNumber}</GeogaphyIdentityNumber>
									          </Geographies>
		          						    <TimeFrames>
								            <StartDate>${StartDate}</StartDate>
								            <StopDate>${StopDate}</StopDate>
								            <TimeFrameType>${TimeFrameType}</TimeFrameType>
								          </TimeFrames>
		                                  </Commitment>
		                 				<MasterContractNumber>${MasterContractNumber}</MasterContractNumber>
								        <ProposalNumber>${ProposalNumber}</ProposalNumber>
								        <ClientSignature>${ClientSignature}</ClientSignature>
								        <ClientSignarureDate>${ClientSignarureDate}</ClientSignarureDate>
								        <ClientSignatureTitle></ClientSignatureTitle>
								        <CatalinaSignature></CatalinaSignature>
								        <CatalinaSignatureDate>${CatalinaSignatureDate}</CatalinaSignatureDate>
								        <CatalinaSignatureTitle></CatalinaSignatureTitle>
								        <UserName>${UserName}</UserName>
		                				<StoreQuantity>${StoreQuantity}</StoreQuantity>
								        <EstPrintQuantity>${EstPrintQuantity}</EstPrintQuantity>
								        <MinimumAmount>${MinimumAmount}</MinimumAmount>
								        <MinPrintsIncluded>${MinPrintsIncluded}</MinPrintsIncluded>
		                				<ProgramSetUpFee>${ProgramSetUpFee}</ProgramSetUpFee>
								        <BillContactKey></BillContactKey>
								        <BillContactFirstName>${BillContactFirstName}</BillContactFirstName>
								        <BillContactLastName>${BillContactLastName}</BillContactLastName>
								        <MiscellaneousCharges>${MiscellaneousCharges}</MiscellaneousCharges>
								        <Header>
								          <RequestingSystem xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate"></RequestingSystem>
								          <CallFromFunction xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate"></CallFromFunction>
								          <User xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate"></User>
								         </Header>
								      </request>
								   </saveContract>
								</Body>
				 </Envelope>"""
		
			//set soap body and send request and verify status code
			request.setSoapBody(strSOAPBody)
			def response = WS.sendRequest(request)
			int StatusCode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response, 200,"Verify_saveContract_Details")
			logger.info("status code:"+StatusCode)
		
						
			//Validate Fields by passing null 
			XmlParser slurper = new XmlParser()
			def parsedXml = slurper.parseText(response.getResponseBodyContent())
			logger.info(parsedXml)
			def ErrorMessage =parsedXml.saveContractResult.ErrorMessage.text()
			logger.info("ErrorMessage is  :" + ErrorMessage)
			def ActualErroMessage= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,i,37)
			
			if (ActualErroMessage == ErrorMessage) {
				logger.info("Expected and actual ActualErroMessage match")
				assert true
			} else {
				logger.error("Expected and actual ActualErroMessage do not match: Expected "+ ErrorMessage + ' Actual ' + ActualErroMessage)
				com.utility.UpdateTargetProcess api = new com.utility.UpdateTargetProcess()
				api.UpdateTestCaseStatus(testCaseName, "Failed")
				assert false
			}
			
			CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_MandatoryFields_SaveContract_Details", "Passed")
			logger.info("***********************************End test case for MandatoryFields ***************************************")

}