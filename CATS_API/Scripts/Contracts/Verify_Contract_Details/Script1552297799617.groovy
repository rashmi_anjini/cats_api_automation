
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper

CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("getContract")
logger.info("***********************************Start test case for get Contract ***************************************")
def request = (RequestObject)findTestObject('Contracts/getContract')

def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('InsertBill')

def contractNumber= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,1,0)

String strSOAPBody="""<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                        <soap:Body>
                          <getContract xmlns="http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1">
      							<contractNbr>${contractNumber}</contractNbr>
      						    <includeDetails>string</includeDetails>
					    </getContract>
					  </soap:Body>
					</soap:Envelope>"""

	//Set body and send request and verify status code
	request.setSoapBody(strSOAPBody)
	logger.info(request.getSoapBody())
	def response = WS.sendRequest(request)
	int StatusCode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response, 200,"Verify_Contract_Details")
	logger.info("status code:"+StatusCode)

	//parse response get contract details
	XmlParser slurper = new XmlParser()
	def parsedXml = slurper.parseText(response.getResponseBodyContent())
	def Contract= parsedXml.getContractResult.Body[0].text().replace("{{", "{")
	Contract= Contract.replace("}}}","}}")
	JsonSlurper json = new JsonSlurper()
	def contractObj = json.parseText(Contract)
	logger.info("Contract Number: " + contractObj.Contract.ContractNumber)
	logger.info("Contract Key Number : " + contractObj.Contract.MasterKeyNumber)
	logger.info("Start Date : " + contractObj.Contract.StartDate)
	logger.info("Stop Date : " + contractObj.Contract.StopDate)
	
	CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_Contract_Details", "Passed")
	
	
	logger.info("***********************************End test case for get Contract ***************************************")