import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.gargoylesoftware.htmlunit.javascript.host.dom.Document
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS


CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("Insert_Bill")
logger.info("***********************************Start test case for InsertBill ***************************************")
def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('InsertBill')

def contractNumber= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet,1,0)

def request = (RequestObject)findTestObject('Contracts/insertBil')

String strSOAPBody = """<?xml version="1.0" encoding="utf-8"?>

<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<insertBill xmlns="http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1">
			<request>
				<ContractNumber>${contractNumber}</ContractNumber>
 				<Header>   
          		<RequestingSystem xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate">SFDC</RequestingSystem>
          		<CallFromFunction xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate">PDT</CallFromFunction>
          		<User xmlns="http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate">RASH</User>
        		</Header>
      		</request>
    	</insertBill>
  </soap:Body>
</soap:Envelope>"""

		//set body on send request and verify status
		request.setSoapBody(strSOAPBody)
		def response = WS.sendRequest(request)
		int StatusCode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response, 200,"Verify_InsertBill_Details")
		logger.info("status code:"+StatusCode)

		//parse the response and get  bill number
		XmlParser slurper = new XmlParser()
		def parsedXml = slurper.parseText(response.getResponseBodyContent())
		def ContractNumber= parsedXml.insertBillResult[0].ContractNumber.text()
		logger.info("ContractNumber:"+ContractNumber)
		def BillNumber=  parsedXml.insertBillResult[0].BillNumber.text()
		logger.info("BillNumber:"+BillNumber)
		CustomKeywords.'com.utility.WriteExcel.setCellData'(BillNumber, 1, 1, "InsertBill")
		
	
		//validate the Contract number in DB
		def query= CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 1,2)
		String stmt = query+"="+"${BillNumber}"
		logger.info(stmt)
		List records = CustomKeywords.'databaseConnection.execSQL'('TOCUSQA3', '1521', 'TOCUSQA3', 'RANJI', 'RANJI126', stmt)
		int dbCount = records.size()
		logger.info('Databse Count :' + dbCount)
		records.BILL_NBR.toString()
		logger.info('Expected ' + BillNumber + ' Actual '+ records.BILL_NBR)

		CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_InsertBill_Details", "Passed")
		logger.info("***********************************End test case for InsertBill ***************************************")

