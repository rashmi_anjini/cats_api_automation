import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager as LogManager
import org.apache.logging.log4j.core.Logger as Logger

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable as GlobalVariable

//Setup Loggers
CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger('Product_US')


//Get the testcount value from default profile and it iterates through all test cases configured in excel for specific country and write each testcase result to excel
GlobalVariable.testCount = (GlobalVariable.testCount + 1)


def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Valid_Para')
while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 1) != null)
	{
		String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 1)
		String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 2)
		String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 3)
		String query = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 7)


		//sending Request and getting response
		ResponseObject response = WS.sendRequest(findTestObject('Pricing/ProductsClasses', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
		            , ('managingCountryCode') : managingCountryCode]))
		
		logger.info(((((((('*****************Start test case no : ' + GlobalVariable.testCount) + ' for ActiveFlag: ') + activeOnly) + 
		    ', GlobalFlag :') + globalsOnly) + ', Country :') + managingCountryCode) + '*****************************')
		
		int statuscode = CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response, 200,"Verify_PC_StatusCode_and_Response_US")
		
		//write status code to excel in valid_para sheet
		CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCount, 4, 'Valid_Para')
		
		//get the query from excel and get the count of products
		String stmt = query
		
		logger.info(stmt)
		
		List records = CustomKeywords.'databaseConnection.execSQL'('TOCUSQA3', '1521', 'TOCUSQA3', 'RANJI', 'RANJI126', stmt)
		
		int dbCount = records.size()
		
		logger.info('Databse Count :' + dbCount)
		
		//write db count to Excel in valid_para sheet
		CustomKeywords.'com.utility.WriteExcel.setCellData'(dbCount, GlobalVariable.testCount, 6, 'Valid_Para')
		
		//Verifying response count 	
		int responsecount = CustomKeywords.'com.utility.CommonMethods.verifyObjectCount'(response, dbCount)
		
		//write response count to Excel in valid_para sheet
		CustomKeywords.'com.utility.WriteExcel.setCellData'(responsecount, GlobalVariable.testCount, 5, 'Valid_Para')
		
		List parsedData = CustomKeywords.'com.utility.CommonMethods.parseResponseJson'(response)
		
		//validating elements both in db and response
		CustomKeywords.'com.utility.CommonMethods.VerifyElementsinDatabaseandResponse'(parsedData, responsecount, dbCount, records, "Verify_PC_StatusCode_and_Response_US")
		//GlobalVariable.testCount = (GlobalVariable.testCount + 1)
		CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_PC_StatusCode_and_Response_US", "Passed")
		logger.info(('***********************End test case no ' + GlobalVariable.testCount) + '*****************************')
		GlobalVariable.testCount = (GlobalVariable.testCount + 1)


}

