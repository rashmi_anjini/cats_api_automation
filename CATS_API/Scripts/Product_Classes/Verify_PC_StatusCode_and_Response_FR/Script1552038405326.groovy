import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable as GlobalVariable


//Setup Loggers
CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("Product_FR")

GlobalVariable.testCountFR = GlobalVariable.testCountFR + 1

def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Valid_Para')
while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountFR, 1) != null)
	{

	
		String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountFR, 1)
		String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountFR, 2)
		String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountFR, 3)
		String query = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountFR, 7)

	
		ResponseObject response = WS.sendRequest(findTestObject('Pricing/ProductsClasses', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
	, ('managingCountryCode') : managingCountryCode]))
		
		logger.info("*****************Start test case no : " + GlobalVariable.testCountFR +" for ActiveFlag: " +activeOnly + ", GlobalFlag :"+ globalsOnly+", Country :"+ managingCountryCode+"*****************************")
		
		List parsedData = CustomKeywords.'com.utility.CommonMethods.parseResponseJson'(response)
		
		int statuscode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response,200,"Verify_PC_StatusCode_and_Response_FR")
		
		CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCountFR ,4,'Valid_Para')
	 
	      
		String stmt = query
	  
		logger.info(stmt)
	   
	   	List records = CustomKeywords.'databaseConnection.execSQL'('TOCFRQA3', '1521', 'TOCFRQA3', 'RANJI', 'RANJI126', stmt)
	   int dbCount= records.size()
	   
	   	logger.info('Databse Count :' + dbCount)
	   
	   	CustomKeywords.'com.utility.WriteExcel.setCellData'(dbCount, GlobalVariable.testCountFR , 6,'Valid_Para')
	  
		 int responsecount=  CustomKeywords.'com.utility.CommonMethods.verifyObjectCount'(response, dbCount)
		 logger.info('responsecount :' + responsecount)
		
		 CustomKeywords.'com.utility.WriteExcel.setCellData'(responsecount, GlobalVariable.testCountFR , 5,'Valid_Para')
	   
			 //validating elements both in db and response
		 CustomKeywords.'com.utility.CommonMethods.VerifyElementsinDatabaseandResponse'(parsedData, responsecount, dbCount, records,"Verify_PC_StatusCode_and_Response_FR")
		
		 logger.info("***********************End test case no " + GlobalVariable.testCountFR +"*****************************")
		CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_PC_StatusCode_and_Response_FR", "Passed")
		GlobalVariable.testCountFR = GlobalVariable.testCountFR + 1
}


