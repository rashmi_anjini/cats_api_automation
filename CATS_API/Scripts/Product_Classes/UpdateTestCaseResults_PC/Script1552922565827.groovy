import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper as JsonSlurper
import internal.GlobalVariable as GlobalVariable




CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("TP_ProdClasses")


		logger.info("******************************Create Test Plan run Id***********************************************")		
		def dateTime= CustomKeywords.'com.utility.CommonMethods.simpleDateTimeFormat'()
		
		//Create TestPlanrun
		def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('TP_data')
		String TestPlanName = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 1, 0)
		String TestPlanId = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, 1, 1)
		
		def request = (RequestObject)findTestObject('TargetProcessEndPoints/CreateTestRuns', [('access_token') : GlobalVariable.access_token])
					
		String strbody= """{"Name":"${TestPlanName+dateTime}", "TestPlan":{"Id":${TestPlanId} }}"""

	    logger.info("Body:" +strbody)

		request.setBodyContent(new HttpTextBodyContent(strbody,"UTF-8", "application/json"))
	    def response=	WS.sendRequest(request)
	
		int StatusCode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response, 201,"CreateTestPlanRun")
		JsonSlurper slurper = new JsonSlurper()
		
		def parsedJson = slurper.parseText(response.getResponseBodyContent())
		
		def testPlanRunId = parsedJson.Id
		logger.info("testplanrunid:" +testPlanRunId)
		logger.info("******************************Get Test Case run Id's for each test case***********************************************")
		//GetTestCasesRuns
		def response1 = WS.sendRequest(findTestObject('TargetProcessEndPoints/GetTestcaseRunsById', [('access_token') : GlobalVariable.access_token
		            , ('TestPlanRunId') : testPlanRunId]))
		
		logger.info("Request Information : https://catalina.tpondemand.com/api/v1/TestPlanRuns/"+testPlanRunId+"/TestCaseRuns/?access_token="+GlobalVariable.access_token  )
		
		def parsedJson1 = slurper.parseText(response1.getResponseBodyContent())
		
		int count = parsedJson1.Items.size()

			for (int i = 0; i < count; i++) 
				{
		
				    int testCaseRunId = parsedJson1.Items[i].Id
				
				    GlobalVariable.TestCaseStatus.put(parsedJson1.Items[i].TestCase.Name.toString(), testCaseRunId)
					
				    //def result = WS.callTestCase(findTestCase((parsedJson1.Items[i].TestPlanRun.Name + '/') + parsedJson1.Items[i].TestCase.Name.toString()),null, FailureHandling.CONTINUE_ON_FAILURE)
					logger.info("Test Case Name: "+parsedJson1.Items[i].TestCase.Name)
					logger.info("Test Case Run Id: "+testCaseRunId.toString())
					
				}
		WS.delay(5)
	
