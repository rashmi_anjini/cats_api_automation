import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable as GlobalVariable

		CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
		Logger logger = LogManager.getLogger("PCInvalidPara")
		
		GlobalVariable.testCountIN = GlobalVariable.testCountIN + 1

		
		def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Invalid_Para')
		
		while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 1) != null)
		{
		logger.info("*************************Start test case no : " + GlobalVariable.testCountIN + " Invalid_Parameters for Product Classes************************************")
		String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 1)
		String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 2)
		String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 3)
		
		
		ResponseObject response = WS.sendRequest(findTestObject('Pricing/ProductsClasses', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
	, ('managingCountryCode') : managingCountryCode]))
		
		logger.info("Request Information : http://c2capi-sqa.catmktg.com/api/Products/GetAllProducts/"+activeOnly+ "/" + globalsOnly +"?managingCountryCode="+managingCountryCode  )
		
		int expectedSatusCode=500
		
		int statuscode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response,expectedSatusCode,"Verify_PC_StatusCode_For_Invalid_Parameters")
		
		
	   CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCountIN ,4,'Invalid_Para')	   
	   
	   CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_PC_StatusCode_For_Invalid_Parameters", "Passed")
	   GlobalVariable.testCountIN = GlobalVariable.testCountIN + 1
		}
	   