import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.apache.logging.log4j.core.LoggerContext
import org.omg.CORBA.portable.CustomValue

import internal.GlobalVariable

//Setup Loggers
CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("Product_UK")

GlobalVariable.testCountUK = GlobalVariable.testCountUK + 1
		
		
def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Valid_Para')
while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountUK, 1) != null)
		{
		
		String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountUK, 1)
		String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountUK, 2)
		String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountUK, 3)
		String query = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountUK, 7)
		
		ResponseObject response = WS.sendRequest(findTestObject('Pricing/ProductsClasses', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
	, ('managingCountryCode') : managingCountryCode]))
		logger.info("*****************Start test case no : " + GlobalVariable.testCountUK +" for ActiveFlag: " +activeOnly + ", GlobalFlag :"+ globalsOnly+", Country :"+ managingCountryCode+"*****************************")
		
		List parsedData = CustomKeywords.'com.utility.CommonMethods.parseResponseJson'(response)
		
		int statuscode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response,200,"Verify_PC_StatusCode_and_Response_UK")
		
		CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCountUK ,4, 'Valid_Para')
	 
	  
	   
		String stmt = query
	  
		logger.info(stmt)
	  
		List records = CustomKeywords.'databaseConnection.execSQL'('TOCUKQA3', '1521', 'TOCUKQA3', 'RANJI', 'RANJI126', stmt)
	  
	  
		int dbCount= records.size()
	   
		logger.info('Databse Count :' + dbCount)
	  
		CustomKeywords.'com.utility.WriteExcel.setCellData'(dbCount, GlobalVariable.testCountUK , 6,'Valid_Para')
	  		 
	
		 int responsecount=  CustomKeywords.'com.utility.CommonMethods.verifyObjectCount'(response, dbCount)
		 logger.info('responsecount :' + responsecount)
		 CustomKeywords.'com.utility.WriteExcel.setCellData'(responsecount, GlobalVariable.testCountUK , 5,'Valid_Para')
		
		 
		//validating elements both in db and response
		 CustomKeywords.'com.utility.CommonMethods.VerifyElementsinDatabaseandResponse'(parsedData, responsecount, dbCount, records,"Verify_PC_StatusCode_and_Response_UK")
		 
		 CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_PC_StatusCode_and_Response_UK", "Passed")
		 logger.info("***********************End test case no " + GlobalVariable.testCountUK +"*****************************")
		GlobalVariable.testCountUK = GlobalVariable.testCountUK + 1
		
}
   
		
   


