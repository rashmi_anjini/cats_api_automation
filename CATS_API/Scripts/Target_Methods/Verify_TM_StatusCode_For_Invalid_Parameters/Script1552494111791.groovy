import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable as GlobalVariable



//Setup Loggers
CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("TMInvalidPara")

//Get data from excel	
def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Invalid_Para_TM')

//Set the row index for getting test data from excel	
GlobalVariable.testCountIN = 1

//Execute each test case from Invalid parameter test case	
while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 1) != null)
{
	logger.info("*********************Start test case no :" + GlobalVariable.testCountIN + " Invalid_Parameters for Target Methods***********************")
		
	String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 1)
	String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 2)
	String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountIN, 3)

	ResponseObject response = WS.sendRequest(findTestObject('Pricing/TargetMethods', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
, ('managingCountryCode') : managingCountryCode]))
	
	int expectedSatusCode=500
	
	int statuscode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response,expectedSatusCode,"Verify_TM_StatusCode_For_Invalid_Parameters")
	
	
   CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCountIN ,4,'Invalid_Para_TM')
   CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_TM_StatusCode_For_Invalid_Parameters", "Passed")
   logger.info("*********************End test case no :" + GlobalVariable.testCountIN +"***************************")
   GlobalVariable.testCountIN = GlobalVariable.testCountIN + 1
   
}