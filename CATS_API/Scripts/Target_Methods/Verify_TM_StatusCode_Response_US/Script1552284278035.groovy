import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable as GlobalVariable

		//Setup Loggers
		CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
		Logger logger = LogManager.getLogger("TrgtMthds_US")
		def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Valid_Para_TM')
		
		//Reset the start index to first row of US country record.
		GlobalVariable.testCount = 1
	
				
		while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 1) != null)
		{
		String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 1)
		String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 2)
		String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 3)
		String query = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCount, 7)
				
		// get the status code and write it to excel
		ResponseObject response = WS.sendRequest(findTestObject('Pricing/TargetMethods', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
	, ('managingCountryCode') : managingCountryCode]))
		
		logger.info("*****************Start test case no : " + GlobalVariable.testCount +" for ActiveFlag: " +activeOnly + ", GlobalFlag :"+ globalsOnly+", Country :"+ managingCountryCode+"*****************************")
		
		List parsedData = CustomKeywords.'com.utility.CommonMethods.parseResponseJson'(response)
		
		int statuscode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response,200,'Verify_TM_StatusCode_Response_US')
		
	    CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCount , 4, 'Valid_Para_TM')
	 
	   
	   // this below codes gets the  counts of number of  Targetsing Methods in database 
		String stmt = query
	  
		logger.info(stmt)
	  
		List records = CustomKeywords.'databaseConnection.execSQL'('TOCUSQA3', '1521', 'TOCUSQA3', 'RANJI', 'RANJI126', stmt)
		int dbCount= records.size()
		logger.info("dbc ount: " +dbCount)
	   
	   //writing the count to the excel
	   	CustomKeywords.'com.utility.WriteExcel.setCellData'(dbCount, GlobalVariable.testCount , 6, 'Valid_Para_TM')
	  
		  
		 //this below codes gets the counts of objects in response
		 int responsecount=  CustomKeywords.'com.utility.CommonMethods.verifyObjectCount'(response, dbCount)
		 
		
		 CustomKeywords.'com.utility.WriteExcel.setCellData'(responsecount, GlobalVariable.testCount , 5, 'Valid_Para_TM')
   
		 CustomKeywords.'com.utility.CommonMethods.VerifyElementsinTargetmethods'(parsedData, responsecount, dbCount, records,'Verify_TM_StatusCode_Response_US')
	   
		 CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_TM_StatusCode_Response_US", "Passed")
		 
		 logger.info("***********************End test case no " + GlobalVariable.testCount +"*****************************")
		
		 GlobalVariable.testCount = GlobalVariable.testCount + 1
		}


