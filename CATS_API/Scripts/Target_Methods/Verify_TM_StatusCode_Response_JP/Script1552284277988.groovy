import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable as GlobalVariable

//Setup Loggers
CustomKeywords.'com.utility.CommonMethods.setUpLoggers'()
Logger logger = LogManager.getLogger("TrgtMthds_JP")
		
//Reset the start index to first row of JP country record.
		GlobalVariable.testCountJP = 22
		def sheet = CustomKeywords.'com.utility.ReadExcel.getSheet'('Valid_Para_TM')
				
while(CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountJP, 1) != null)
	{			
		
		String activeOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountJP, 1)
		String globalsOnly = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountJP, 2)
		String managingCountryCode = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountJP, 3)
		String query = CustomKeywords.'com.utility.ReadExcel.getCellData'(sheet, GlobalVariable.testCountJP, 7)
		
		
		// get the status code and write it to excel
		ResponseObject response = WS.sendRequest(findTestObject('Pricing/TargetMethods', [('activeOnly') : activeOnly, ('globalsOnly') : globalsOnly
	, ('managingCountryCode') : managingCountryCode]))
		
		logger.info("*****************Start test case no : " + GlobalVariable.testCountJP +" for ActiveFlag: " +activeOnly + ", GlobalFlag :"+ globalsOnly+", Country :"+ managingCountryCode+"*****************************")
		
		List parsedData = CustomKeywords.'com.utility.CommonMethods.parseResponseJson'(response)
		
		int statuscode= CustomKeywords.'com.utility.CommonMethods.verifyStatusCode'(response,200,'Verify_TM_StatusCode_Response_JP')
		
	    CustomKeywords.'com.utility.WriteExcel.setCellData'(statuscode, GlobalVariable.testCountJP , 4, 'Valid_Para_TM')
	 
	   
	   // this below codes gets the  counts of number of  Targetsing Methods in database 
		String stmt = query
	  
		logger.info(stmt)
	  
		List records = CustomKeywords.'databaseConnection.execSQL'('TOCJPQA3', '1521', 'TOCJPQA3', 'RANJI', 'RANJI126', stmt)
		int dbCount= records.size()
		logger.info("db count: " +dbCount)
	   
	   //writing the count to the excel
	   	CustomKeywords.'com.utility.WriteExcel.setCellData'(dbCount, GlobalVariable.testCountJP , 6, 'Valid_Para_TM')
	  
		  
		 //this below codes gets the counts of objects in response
		 int responsecount=  CustomKeywords.'com.utility.CommonMethods.verifyObjectCount'(response, dbCount)
		 
		
		 CustomKeywords.'com.utility.WriteExcel.setCellData'(responsecount, GlobalVariable.testCountJP , 5, 'Valid_Para_TM')
   
		 CustomKeywords.'com.utility.CommonMethods.VerifyElementsinTargetmethods'(parsedData, responsecount, dbCount, records,'Verify_TM_StatusCode_Response_JP')
	     CustomKeywords.'com.utility.UpdateTargetProcess.UpdateTestCaseStatus'("Verify_TM_StatusCode_Response_JP", "Passed")
		 
		 logger.info("***********************End test case no " + GlobalVariable.testCountJP +"*****************************")
   
		 GlobalVariable.testCountJP = GlobalVariable.testCountJP + 1
		 }

