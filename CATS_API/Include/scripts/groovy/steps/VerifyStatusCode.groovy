package steps

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS



class VerifyStatusCode {

	static response


	@Given('^The APi service is available$')

	def the_api_service_is_available() {
		@Keyword
				response= WS.callTestCase(findTestCase('Test Cases/ProductClasses/T001_Verify_the_StatusCode_With_Valid_Parameters'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}