Feature: Status code validation
  
   Scenario: Verify Status code in the response
    Given The APi service is available 
    When I get information of an issue with Id "ProdNumber"
    Then I get response code "200"