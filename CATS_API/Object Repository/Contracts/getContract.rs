<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>getContract</name>
   <tag></tag>
   <elementGuidId>bc663001-5d25-4c88-9c58-6d037c7a9777</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?>
&lt;soap:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soap=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot;>
  &lt;soap:Body>
    &lt;getContract xmlns=&quot;http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1&quot;>
      &lt;contractNbr>172592&lt;/contractNbr>
      &lt;includeDetails>string&lt;/includeDetails>
    &lt;/getContract>
  &lt;/soap:Body>
&lt;/soap:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>getContract</soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()






WS.verifyElementText(response, 'getContractResponse.getContractResult.Body', '{&quot;Contract&quot;:{{&quot;ClonedFromKey&quot;:0,&quot;ReceivedDate&quot;:&quot;3/7/2019&quot;,&quot;ClientKeyNumber&quot;:10144,&quot;ContractDescription&quot;:&quot;API automation&quot;,&quot;BusinessSegmentNumber&quot;:1,&quot;RevenueBusinessSegmentNumber&quot;:1,&quot;BillingPrdKeyNbr&quot;:1,&quot;ContractStatCode&quot;:&quot;A&quot;,&quot;CeilingLevelCode&quot;:&quot;C&quot;,&quot;Filter&quot;:{&quot;Obj&quot;:{&quot;ObjDescr&quot;:&quot;&quot;,&quot;ObjTypCd&quot;:&quot;C&quot;,&quot;ObjKeyNbr&quot;:0,&quot;HasObj&quot;:false,&quot;EqualityId&quot;:&quot;C_0&quot;,&quot;ObjectKey&quot;:&quot;84ca05b6-21db-4399-9d25-6eee68e70334&quot;,&quot;IsDirty2&quot;:false},&quot;IsParent&quot;:false,&quot;ValidateOverlaps&quot;:true,&quot;FilterKeyNbr&quot;:0,&quot;FilterDetailCollection&quot;:[],&quot;Parent&quot;:{&quot;Obj&quot;:{&quot;ObjDescr&quot;:&quot;&quot;,&quot;ObjTypCd&quot;:&quot;&quot;,&quot;ObjKeyNbr&quot;:0,&quot;HasObj&quot;:false,&quot;EqualityId&quot;:&quot;_0&quot;,&quot;ObjectKey&quot;:&quot;1786d90d-432d-4926-b8da-130e282aed3a&quot;,&quot;IsDirty2&quot;:false},&quot;IsParent&quot;:true,&quot;ValidateOverlaps&quot;:true,&quot;FilterKeyNbr&quot;:0,&quot;FilterDetailCollection&quot;:[],&quot;Parent&quot;:null,&quot;FilterDetailCollectionActive&quot;:[],&quot;StartDate&quot;:null,&quot;StopDate&quot;:null,&quot;FilterTypDescr&quot;:&quot;(none)&quot;,&quot;FilterShortDescr&quot;:&quot;(none)&quot;,&quot;FilterDescr&quot;:&quot;(none)&quot;,&quot;ObjectKey&quot;:&quot;c3236dcc-d39c-49c7-ba25-057d4724527b&quot;,&quot;IsDirty2&quot;:false},&quot;FilterDetailCollectionActive&quot;:[],&quot;StartDate&quot;:null,&quot;StopDate&quot;:null,&quot;FilterTypDescr&quot;:&quot;(none)&quot;,&quot;FilterShortDescr&quot;:&quot;(none)&quot;,&quot;FilterDescr&quot;:&quot;(none)&quot;,&quot;ObjectKey&quot;:&quot;caff6cea-b7b2-498c-b5c7-095ef10fa372&quot;,&quot;IsDirty2&quot;:false},&quot;AcctSpecificFlag&quot;:false,&quot;ContractKeyNumber&quot;:172592,&quot;ContractNumber&quot;:172592,&quot;ContractDate&quot;:&quot;3/7/2019&quot;,&quot;OriginalClientKeyNumber&quot;:10144,&quot;LastLoadedClientKeyNumber&quot;:0,&quot;StartDate&quot;:&quot;3/7/2019&quot;,&quot;StopDate&quot;:&quot;&quot;,&quot;ExpirationNotifDays&quot;:120,&quot;StandardContractFlg&quot;:true,&quot;PublicityFlag&quot;:false,&quot;EvergreenClauseFlag&quot;:false,&quot;EvergreenYearNumber&quot;:0,&quot;EvergreenDays&quot;:120,&quot;MasterAgreementNumber&quot;:0,&quot;MasterKeyNumber&quot;:0,&quot;OriginalMasterKeyNumber&quot;:0,&quot;HasMaster&quot;:false,&quot;PO1&quot;:&quot;&quot;,&quot;PO2&quot;:&quot;&quot;,&quot;PO3&quot;:&quot;&quot;,&quot;VolumeDiscountFlag&quot;:false,&quot;ProgramFeeAdjustment&quot;:0.0,&quot;OtherFeeAdjustment&quot;:0.0,&quot;ContractAmount&quot;:0.0,&quot;UptoAmount&quot;:0.0,&quot;OverrideAmount&quot;:0.0,&quot;OveragePercent&quot;:10,&quot;FinanceChargePercent&quot;:1.50,&quot;BillTermsNoteNumber&quot;:&quot;&quot;,&quot;RecognizedRevenueAmount&quot;:0.0,&quot;SpecialCommissionFlag&quot;:false,&quot;SpecialCommissionComment&quot;:&quot;&quot;,&quot;NPRSServiceTypeCode&quot;:&quot;&quot;,&quot;AddOnDiscountRateAmount&quot;:0.01,&quot;EstimatedMessageNumber&quot;:0,&quot;NonNationalUpchargeFlag&quot;:false,&quot;MinimumPrintsIncludedNumber&quot;:0,&quot;MinimumAmount&quot;:0.0,&quot;FlatFee&quot;:0.0,&quot;VarDescription&quot;:&quot;&quot;,&quot;ChangeUserCode&quot;:&quot;&quot;,&quot;ProposalNbr&quot;:0,&quot;CampaignNumber&quot;:0,&quot;TaxFlag&quot;:false,&quot;EstProgramFee&quot;:0.0,&quot;SummaryInvFlg&quot;:false,&quot;PORequiredFlag&quot;:false,&quot;ExternalAgreementNumber&quot;:&quot;&quot;,&quot;IsMasterAgreement&quot;:false,&quot;RequestTypeKeyNumber&quot;:null,&quot;RequestTypeShortName&quot;:&quot;&quot;,&quot;RequestTypeDescription&quot;:&quot;&quot;,&quot;Active&quot;:true,&quot;InvoiceScheduleRuleCollection&quot;:[],&quot;ContractContactCollection&quot;:[],&quot;ContractAssociatedPersonSet&quot;:{&quot;CorporateClientServicesContact&quot;:{&quot;ContractAssociatedPersonKeyNumber&quot;:0,&quot;ContractKeyNumber&quot;:0,&quot;AssociateTypeCode&quot;:&quot;C&quot;,&quot;UserIDText&quot;:&quot;&quot;,&quot;AssociateDescription&quot;:&quot;&quot;,&quot;SourceTypeCode&quot;:&quot;A&quot;,&quot;ObjectKey&quot;:&quot;1e135b46-d813-481c-b93a-d46c84ad677e&quot;,&quot;IsDirty2&quot;:false},&quot;RegionalClientServicesContact&quot;:{&quot;ContractAssociatedPersonKeyNumber&quot;:0,&quot;ContractKeyNumber&quot;:0,&quot;AssociateTypeCode&quot;:&quot;R&quot;,&quot;UserIDText&quot;:&quot;&quot;,&quot;AssociateDescription&quot;:&quot;&quot;,&quot;SourceTypeCode&quot;:&quot;A&quot;,&quot;ObjectKey&quot;:&quot;714af6f1-7394-4e88-a8f0-9b7d6f813cf1&quot;,&quot;IsDirty2&quot;:false},&quot;Manager&quot;:{&quot;ContractAssociatedPersonKeyNumber&quot;:0,&quot;ContractKeyNumber&quot;:0,&quot;AssociateTypeCode&quot;:&quot;S&quot;,&quot;UserIDText&quot;:&quot;&quot;,&quot;AssociateDescription&quot;:&quot;&quot;,&quot;SourceTypeCode&quot;:&quot;A&quot;,&quot;ObjectKey&quot;:&quot;cfe6619e-c944-4434-8cee-3ee8f5cac49c&quot;,&quot;IsDirty2&quot;:false},&quot;ObjectKey&quot;:&quot;a90ac910-34be-4878-88f9-27bf7ae3c130&quot;,&quot;IsDirty2&quot;:false},&quot;ContractSignatureSet&quot;:[],&quot;NotesCollection&quot;:[],&quot;AttachmentCollection&quot;:[],&quot;ContractProductServicePriceCollection&quot;:[],&quot;ContractProductClassPriceCollection&quot;:[],&quot;ContractColorPriceCollection&quot;:[],&quot;ContractTargetingMethodPriceCollection&quot;:[],&quot;ContractBillAttributePriceCollection&quot;:[],&quot;ContractAddonPriceCollection&quot;:[],&quot;ContractAgreementCollection&quot;:[],&quot;ContractNonStandardReasonCodeCollection&quot;:[],&quot;ContractProductClassesCollection&quot;:[],&quot;ContractProductServicesCollection&quot;:[],&quot;RetailContractCollection&quot;:[],&quot;ContractSalesPersonCollection&quot;:[],&quot;SalesHierarchyCollection&quot;:[],&quot;SalesHierearchyNotesCollection&quot;:[],&quot;ContractReferenceCollection&quot;:[],&quot;ClientCollection&quot;:[],&quot;BusinessUnitCollection&quot;:[],&quot;CountryCollection&quot;:[],&quot;ContractTierCollection&quot;:[],&quot;SpecificationObjectCol&quot;:[],&quot;ObjectKey&quot;:&quot;2a342512-b5a9-4268-ba1f-3b6b022e0b78&quot;,&quot;IsDirty2&quot;:false}}}')</verificationScript>
   <wsdlAddress>http://stptc2ccatv01/CATS-Intl-SQA/FinancialSystemsWebServices/Contracts.asmx?wsdl</wsdlAddress>
</WebServiceRequestEntity>
