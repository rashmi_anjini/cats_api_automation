<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>saveContract</name>
   <tag></tag>
   <elementGuidId>938fbc96-072b-4c5d-8aac-be86eb3ab9df</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>text/xml</value>
   </httpHeaderProperties>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;Envelope xmlns=&quot;http://www.w3.org/2003/05/soap-envelope&quot;>
    &lt;Body>
        &lt;saveContract xmlns=&quot;http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1&quot;>
            
            &lt;request>
        &lt;ContractDescription>API TESTING&lt;/ContractDescription>
        &lt;AgreementTypeNumber>20&lt;/AgreementTypeNumber>
        &lt;ClientNumber>775&lt;/ClientNumber>
        &lt;SalesPersonNumber>1592&lt;/SalesPersonNumber>
        &lt;ContractAmount>1240&lt;/ContractAmount>
        &lt;ContractStartDate>2019-03-07&lt;/ContractStartDate>
        &lt;ContractStopDate>2019-06-07&lt;/ContractStopDate>
        &lt;Commitment>
          
          &lt;ExclusivityType>T&lt;/ExclusivityType>
          &lt;CategoryOwnership>W&lt;/CategoryOwnership>
          &lt;Categories>501&lt;/Categories>
          
          &lt;Chains>&lt;/Chains>
          
          &lt;Geographies>
            &lt;GeographyType>C&lt;/GeographyType>
            &lt;GeogaphyIdentityNumber>387&lt;/GeogaphyIdentityNumber>
          &lt;/Geographies>
          
          &lt;TimeFrames>
            &lt;StartDate>2019-01-07&lt;/StartDate>
            &lt;StopDate>2019-01-07&lt;/StopDate>
            &lt;TimeFrameType>Cycle&lt;/TimeFrameType>
          &lt;/TimeFrames>
          
        &lt;/Commitment>
         
        &lt;MasterContractNumber>172592&lt;/MasterContractNumber>
        &lt;ProposalNumber>96584&lt;/ProposalNumber>
        &lt;ClientSignature>MTango&lt;/ClientSignature>
        &lt;ClientSignarureDate>2019-01-07&lt;/ClientSignarureDate>
        &lt;ClientSignatureTitle>&lt;/ClientSignatureTitle>
        &lt;CatalinaSignature>&lt;/CatalinaSignature>
        &lt;CatalinaSignatureDate>2019-01-07&lt;/CatalinaSignatureDate>
        &lt;CatalinaSignatureTitle>&lt;/CatalinaSignatureTitle>
        &lt;UserName>ranji&lt;/UserName>
        
        &lt;StoreQuantity>0&lt;/StoreQuantity>
        &lt;EstPrintQuantity>0&lt;/EstPrintQuantity>
        &lt;MinimumAmount>0&lt;/MinimumAmount>
        &lt;MinPrintsIncluded>0&lt;/MinPrintsIncluded>
        
        &lt;ProgramSetUpFee>28&lt;/ProgramSetUpFee>
        &lt;BillContactKey>&lt;/BillContactKey>
        &lt;BillContactFirstName>Frederick&lt;/BillContactFirstName>
        &lt;BillContactLastName>&lt;/BillContactLastName>
        &lt;MiscellaneousCharges>287&lt;/MiscellaneousCharges>
        &lt;Header>
          &lt;RequestingSystem xmlns=&quot;http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate&quot;>&lt;/RequestingSystem>
          &lt;CallFromFunction xmlns=&quot;http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate&quot;>&lt;/CallFromFunction>
          &lt;User xmlns=&quot;http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate&quot;>&lt;/User>
         &lt;/Header>
            &lt;/request>
        &lt;/saveContract>
    &lt;/Body>
&lt;/Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>saveContract</soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress>http://stptc2ccatv01/CATS-Intl-SQA/FinancialSystemsWebServices/Contracts.asmx?wsdl</wsdlAddress>
</WebServiceRequestEntity>
