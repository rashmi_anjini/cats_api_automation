<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>insertBil</name>
   <tag></tag>
   <elementGuidId>52e35e86-4f66-417a-9c96-974a9635c07e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>text/xml</value>
   </httpHeaderProperties>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?>

&lt;soap:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soap=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot;>
	&lt;soap:Body>
		&lt;insertBill xmlns=&quot;http://catalinamarketing.com/FinancialServices/FinancialServicesWebServices/Contracts/v1&quot;>
			&lt;request>
				&lt;ContractNumber>long&lt;/ContractNumber>
 				&lt;Header>   
          		&lt;RequestingSystem xmlns=&quot;http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate&quot;>SFDC&lt;/RequestingSystem>
          		&lt;CallFromFunction xmlns=&quot;http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate&quot;>PDT&lt;/CallFromFunction>
          		&lt;User xmlns=&quot;http://CatalinaMarketing.com/Schemas/Templates/BaseTemplate&quot;>RASH&lt;/User>
        		&lt;/Header>
      		&lt;/request>
    	&lt;/insertBill>
  &lt;/soap:Body>
&lt;/soap:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>insertBill</soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



assertThat(response.getResponseText()).contains('Katalon Test Project')


assertThat(response.getResponseText()).contains('Katalon Test Project')


def jsonSlurper = new JsonSlurper()

def jsonResponse = jsonSlurper.parseText(response.getResponseText())


def jsonSlurper = new JsonSlurper()

def jsonResponse = jsonSlurper.parseText(response.getResponseText())
WS.verifyElementText(response, 'insertBillResponse.insertBillResult.Header.Requestor', 'SFDC')


WS.verifyElementPropertyValue(response, 'issues[0].fields.project.key', 'KTP')




assertThat(response.getResponseText()).isEqualTo(&quot;Katalon Test Project&quot;)



WS.verifyElementText(response, 'insertBillResponse.insertBillResult.BillNumber', '')
WS.verifyElementText(response, 'insertBillResponse.insertBillResult.ContractNumber', '')</verificationScript>
   <wsdlAddress>http://stptc2ccatv01/CATS-Intl-SQA/FinancialSystemsWebServices/Contracts.asmx?wsdl</wsdlAddress>
</WebServiceRequestEntity>
