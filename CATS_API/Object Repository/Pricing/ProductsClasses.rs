<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>ProductsClasses</name>
   <tag></tag>
   <elementGuidId>917f99d1-f121-4a74-b3ac-eacde0135b4d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://c2capi-sqa.catmktg.com/api/Products/GetAllProducts/${active}/${globalsOnly}?managingCountryCode=${managingCountryCode}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import java.util.concurrent.ConcurrentHashMap.MapEntry

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

KeywordLogger logger = new KeywordLogger()

JsonSlurper slurper = new JsonSlurper()
List parsedJson = slurper.parseText(response.getResponseBodyContent())

int count= parsedJson.size()
logger.logInfo(&quot;count of the Products :&quot; + count.toString())

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)


WS.verifyElementPropertyValue(response, '[0].ProdNumber', 2)

WS.verifyElementPropertyValue(response, '[0].CountryCode', &quot;US  &quot;)

WS.verifyElementPropertyValue(response, '[0].StopDate', null)

WS.verifyElementPropertyValue(response, '[0].Description', &quot;CTA - Limited&quot;)


WS.verifyElementPropertyValue(response, '[0].GlobalFlag', false)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
